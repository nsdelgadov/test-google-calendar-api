/**
 * Google API integration plugin.
 *
 * Ussage example:
 * <code>
 *  var plugin = $.fn.gapiExt();
 *
 *  plugin.gApiInit(true, function(authResult){
 *   plugin.printAbout();
 * }, {
 *  clientId: formClientId,
 *  scopes: formScopes
 * });
 * </code>
 * @author adiaz@emergya.com
 */
(function ( $ ) {

    /**
     * Constructor for this plugin
     **/
    $.fn.gapiExt = function( options={formKey: 'undefined'} ) {

        // Extend our default options with those provided.
        // Note that the first argument to extend is an empty
        // object – this is to keep from overriding our "defaults" object.
        opts = $.extend( {}, $.fn.gapiExt.defaults, options );

        // plugin options
        var plugin = $.extend( {}, opts);

        // plugin public functions
        plugin.gApiInit = $.fn.gapiExt.gApiInit;
        plugin.initServices = $.fn.gapiExt.initServices;
        plugin.printAbout = $.fn.gapiExt.printAbout;
        plugin.executeTest = $.fn.gapiExt.executeTest;
        plugin.getGapi = $.fn.gapiExt.getGapi;
        plugin.configBuilder = $.fn.gapiExt.configBuilder;
        plugin.listUpcomingEvents = $.fn.gapiExt.listUpcomingEvents;

        // default properties
        plugin.scopes = [];

        // debug mode
        plugin.debugMode = false;

        // initilize
        plugin.configBuilder(options.formKey);

        // return the plugin instance
        return plugin;
    };


    /**
     * Default configuration for Google API Ext plugin
     **/
    $.fn.gapiExt.defaults = {
        // default gdrive configuration
        gDriveConfig: {
            // see https://developers.google.com/api-client-library/javascript/reference/referencedocs#gapiauthauthorize
            //client_id string  The application's client ID. Visit the Google API Console to get an OAuth 2.0 client ID.
            clientId: '331084783628-0c3fs73le47nav53jnvfgusor4uu7093.apps.googleusercontent.com',
            // scope    string | array  The auth scope or scopes to authorize as a space-delimited string or array (deprecated). Auth scopes for individual APIs can be found in their documentation.
            scopes: 'https://www.googleapis.com/auth/calendar.settings.readonly',
            // immediate    boolean If true, then login uses "immediate mode", which means that the token is refreshed behind the scenes, and no UI is shown to the user.
            immediate: false,
            // response_type    string  The OAuth 2.0 response type property. Default: token
            response_type: 'token'
        }
    };

    /**
     * Initializes Google API and tests for user sign-in, calling a fallback afterwards
     * @param init (true = immediate authorize. Also sets apiKey)
     * @param processResponseFunction (callback function)
     * @param config (optional config parameter. If not found, uses settings from Drupal.settings)
     * @returns {*}
     */
    $.fn.gapiExt.gApiInit = function (init, processResponseFunction, config){
        // this instance
        var me = this;

        // Default settings
        var gDriveConfig = $.extend( {}, me.gDriveConfig, config );
        var returnValue;

        // authorization callback
        var callback = function(oAut2Token){
            // TODO: check oAut2Token status
            me.initServices(function(){
                if(processResponseFunction && typeof(processResponseFunction) == 'function'){
                    processResponseFunction(oAut2Token);
                }
            });
        }

        // save scopes
        this.scopes = gDriveConfig.scopes.split(',');

        // return the authorization
        returnValue = this.getGapi().auth.authorize({
            client_id: gDriveConfig.clientId,
            scope:     gDriveConfig.scopes,
            immediate: gDriveConfig.immediate ? gDriveConfig.immediate : false,
            response_type: gDriveConfig.response_type ? gDriveConfig.response_type : 'token',
            redirect_uri: gDriveConfig.redirect_uri ? gDriveConfig.redirect_uri : 'postmessage'
        }, callback);

        return returnValue;
    }

    /**
     * Initialize all services to be called on JS
     * @param callback
     */
    $.fn.gapiExt.initServices = function (callback){
        // TODO: make it configurable
        // load drive service
        this.getGapi().client.load('calendar', 'v3', function(){
            if(callback && typeof(callback) == 'function'){
                callback();
            }
        });
    }

    $.fn.gapiExt.listUpcomingEvents = function(resultNumber) {
        console.info("listUpcomingEvents");
        var asdf = this.getGapi();
        asdf.client.calendar.events.list({
          'calendarId': 'primary',
          'timeMin': (new Date()).toISOString(),
          'showDeleted': false,
          'singleEvents': true,
          'maxResults': resultNumber,
          'orderBy': 'startTime'
        }).then(function(response) {
          var events = response.result.items;
          console.log(events);
          console.info('Upcoming events:');

          if (events.length > 0) {
            for (i = 0; i < events.length; i++) {
              var event = events[i];
              var when = event.start.dateTime;
              if (!when) {
                when = event.start.date;
              }
              console.info(event.summary + ' (' + when + ')')
            }
          } else {
            console.info('No upcoming events found.');
          }
        });
      }

    /**
     * Print information about the current user along with the Drive API
     * settings.
     * @see https://developers.google.com/drive/v2/reference/about/get
     */
    $.fn.gapiExt.printAbout = function (callback){
        var request = this.getGapi();
        request.client.calendar.events.list({
          'calendarId': 'primary',
          'timeMin': (new Date()).toISOString(),
          'showDeleted': false,
          'singleEvents': true,
          'maxResults': 10,
          'orderBy': 'startTime'
        }).execute(function(resp) {
            console.info("RESP=", resp.result.items);
            if(callback && typeof(callback) == 'function'){
                callback(resp);
            }else{
                var msg = '';
                msg += 'Current user name: ' + resp.name + '\n';
                msg += 'Root folder ID: ' + resp.rootFolderId + '\n';
                msg += 'Total quota (bytes): ' + resp.quotaBytesTotal + '\n';
                msg += 'Used quota (bytes): ' + resp.quotaBytesUsed + '\n';
                console.log(msg);
            }
        });
    };

    $.fn.gapiExt.getGapi = function(){
        // TODO: use debug mode
        if(this.debugMode){
            return gapi_debug;
        }else{
            return gapi;
        }
    };

    $.fn.gapiExt.configBuilder = function (key){
        if(key != 'undefined'){

            // drive config
            var gDriveConfig = {
                'clientId': $(key + '_client_id').val(),
                'scopes': $(key + '_scopes').val(),
                'immediate': $(key + '_immediate').val(),
                'redirect_uri': $(key + '_redirect_uri').val() ? $(key + '_redirect_uri').val() : 'postmessage',
                'response_type': $(key + '_response_type').val() ? $(key + '_response_type').val() : 'token'
            }
            // overwrite default configuration
            this.gDriveConfig = $.extend( {}, this.gDriveConfig, gDriveConfig );
        }

        return this.gDriveConfig;
    }

    /**
     * Perform a simple test for a given configuration
     * @param configuration Google Drive configuration
     */
    $.fn.gapiExt.executeTest = function (configuration){
        var plugin = this;
        plugin.gApiInit(true, function(authResult){
            console.info('Authorization Result:' + JSON.stringify(authResult));
            var printAboutCallback = function(response){
                console.info('printAboutCallback', 'About Result: ' + JSON.stringify(response));
            };
            plugin.printAbout(printAboutCallback);
        }, configuration);
    }

}( jQuery ));